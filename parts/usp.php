<section class="usp">
  <div class="container">
    <div class="row">
      <div class="order-md-2 col-12 col-md-3 usp-photo">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/cta.png" class="rounded-circle">
      </div>
      <div class="order-md-1 col-12 col-md-6 usp-left">
        <span class="usp-text"><?php the_field('usp_text');?></span>
      </div>
      <div class="order-md-3 col-12 col-md-3 usp-right text-right">
        <a href="#" class="cta-btn cta-purple">Afspraak maken</a> 
      </div>
    </div>
  </div>
</section>