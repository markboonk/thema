<header class="banner">
  <div class="container">
    <div class="row">
      <div class="col-8 col-md-3 logo">
        <a class="brand" href="<?= esc_url(home_url('/')); ?>">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/LO_Visiegroep.png">
        </a>
      </div>
      <i class="fas fa-bars hamburger"></i>
      <nav class="col-4 col-md-7">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
        endif;
        ?>
      </nav>
      <div class="col-12 col-md-2 text-right">
        <a href="kennis_maken" class="cta-btn cta-purple cta-top align-self-center">Kennis maken?</a>
      </div>
    </div>
  </div>
</header>
