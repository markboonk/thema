<?php ?>
<div class="container">
  <?php the_content(); ?>
</div>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

<?php get_template_part('parts/breadcrumbs'); ?>

<section class="our-story">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-5 order-md-2 story-img" style="background-color: ;">
        <img src="<?php the_field('text_area_1_image'); ?>" class="rounded-circle img-fluid-350">
      </div>
      <div class="col-12 col-md-7 order-md-1" style="background-color:;">
        <h2 class="section-header"><?php the_field('our_story_title'); ?></h2>
        <p class="text-links"><?php the_field('text_area_1'); ?></p>
      </div>
      <div class="col-12 col-md-5 order-md-3 story-img" style="background-color: ;">
        <img src="<?php the_field('text_area_2_image'); ?>" class="rounded-circle img-fluid-350">
      </div>
      <div class="col-12 col-md-7 text-r order-md-4" style="background-color: ;">
        <p class="text-rechts"><?php the_field('text_area_2'); ?></p>
      </div>
    </div>
  </div>
</section>

<?php get_template_part('parts/team'); ?>
<?php get_template_part('parts/usp'); ?>
