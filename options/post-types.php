<?php
// Verhalen
register_post_type('stories', array(
    'labels' => array(
        'name' => __( 'Stories', 'sage' ),
        'singular_name' => __( 'Story', 'sage' ),
        'add_new' => __( 'Add New', 'sage' ),
        'add_new_item' => __( 'Add new Story', 'sage' ),
        'view_item' => __( 'View Story', 'sage' ),
        'edit_item' => __( 'Edit Story', 'sage' ),
        'new_item' => __( 'New Story', 'sage' ),
        'search_items' => __( 'Search stories', 'sage' ),
        'not_found' =>  __( 'No stories found', 'sage' ),
        'not_found_in_trash' => __( 'No stories found in trash', 'sage' ),
    ),
    'public' => false,
    'has_archive' => false,
    'exclude_from_search' => true,
    'show_ui' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    '_edit_link' => 'post.php?post=%d',
    'rewrite' => array(
        'slug' => 'verhalen',
        'with_front' => true,
    ),
    'query_var' => true,
    'menu_icon' => 'dashicons-format-status',
    'supports' => array( 'title' ),
) );



// Nieuws
register_post_type('news', array(
    'labels' => array(
        'name' => __( 'News', 'sage' ),
        'singular_name' => __( 'News', 'sage' ),
        'add_new' => __( 'Add New', 'sage' ),
        'add_new_item' => __( 'Add new News', 'sage' ),
        'view_item' => __( 'View News', 'sage' ),
        'edit_item' => __( 'Edit News', 'sage' ),
        'new_item' => __( 'New News', 'sage' ),
        'search_items' => __( 'Search News', 'sage' ),
        'not_found' =>  __( 'No news found', 'sage' ),
        'not_found_in_trash' => __( 'No news found in trash', 'sage' ),
    ),
    'public' => true,
    'has_archive' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    '_edit_link' => 'post.php?post=%d',
    'rewrite' => array(
        'slug' => 'nieuws',
        'with_front' => true,
    ),
    'query_var' => true,
    'menu_icon' => 'dashicons-share',
    'supports' => array( 'title' ),
) );

// Team
register_post_type('team', array(
    'labels' => array(
        'name' => __( 'Team', 'sage' ),
        'singular_name' => __( 'Team', 'sage' ),
        'add_new' => __( 'Add New', 'sage' ),
        'add_new_item' => __( 'Add new Team', 'sage' ),
        'view_item' => __( 'View Team', 'sage' ),
        'edit_item' => __( 'Edit Team', 'sage' ),
        'new_item' => __( 'New Team', 'sage' ),
        'search_items' => __( 'Search Team', 'sage' ),
        'not_found' =>  __( 'No team found', 'sage' ),
        'not_found_in_trash' => __( 'No team found in trash', 'sage' ),
    ),
    'public' => false,
    'has_archive' => false,
    'exclude_from_search' => true,
    'show_ui' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    '_edit_link' => 'post.php?post=%d',
    'rewrite' => array(
        'slug' => 'team',
        'with_front' => true,
    ),
    'query_var' => true,
    'menu_icon' => 'dashicons-groups',
    'supports' => array( 'title' ),
) );