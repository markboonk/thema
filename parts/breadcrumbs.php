<?php if (function_exists('yoast_breadcrumb')) : ?>
  <section class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-12 grid-container yoast-breadcrumb" style="background-color:;">
          <?php yoast_breadcrumb('<div id="breadcrumbs">', '</div>'); ?>
        </div>
  </section>
<?php endif; ?>