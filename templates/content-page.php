<?php get_template_part('parts/breadcrumbs'); ?>

<div class="container">
    <div class="row">
        <?php
        $active_sidebar = get_field('activate_sidebar');
        if($active_sidebar){
            $class = "col-md-8";
        } else {
            $class = "col-md-12";
        }
        ?>
        <div class="<?php echo $class; ?>">
            <?php the_content(); ?>
        </div>
        <?php if($active_sidebar): ?>
            <div class="col-md-4">
            <h2 class="section-header list-landing"><?php the_field('promotion_header'); ?></h2>
            <ul class="vg-list" style="list-style-type:disc">
  <?php if (get_field('promotion_1')) : ?><li><?php the_field('promotion_1'); ?></li><?php endif; ?>
  <?php if (get_field('promotion_2')) : ?><li><?php the_field('promotion_2'); ?></li><?php endif; ?>
  <?php if (get_field('promotion_3')) : ?><li><?php the_field('promotion_3'); ?></li><?php endif; ?>
  <?php if (get_field('promotion_4')) : ?><li><?php the_field('promotion_4'); ?></li><?php endif; ?>
</ul>
            <h2 class="section-header callback-landing"><?php the_field('call_request'); ?></h2>            
            <?php echo do_shortcode( '[contact-form-7 id="276" title="Contact form 1"]' ); ?>
            <h2 class="section-header contact-landing"><?php the_field('contact_header'); ?></h2> 
            <?php echo do_shortcode( '[contact-form-7 id="278" title="Contact form 1"]' ); ?>
            </div>
        <?php endif; ?>
    </div class="row">
    <div class= "col-md-12">
   

        </div>
    </div>
</div>

<?php get_template_part('parts/usp'); ?>
