<?php
register_taxonomy(
    'team_category',
    'team',
    array(
        'label' => __( 'Category', 'sage') ,
        'public' => false
    )
);