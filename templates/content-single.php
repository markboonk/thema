<?php get_template_part('parts/breadcrumbs'); ?>

<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>

<section class="news">
  <div class="container">

    <div class="row">
      <div class="col-12 text-center">
        <h2 class="section-header-news-single"><?php the_field('news_title');?></h2>
         
      </div>
    </div>
    <div class="row">
      <div class="col-12 text-center">
      <span class="news-date-single"><?php the_field('news_date');?>
         </span> 
</div>
</div>
    
    <div class="row justify-content-center">

        <?php
        $args = array (
            'posts_per_page' =>  1,
            'post_status' => 'publish',
            'post_type'   => 'news',
            'p' => $post->ID
        );
        $get_posts = get_posts( $args );

        if ( $get_posts ):
            foreach ( $get_posts as $post ): ?>

                <?php
                $title = get_field('news_title');
                $date = get_field('news_date');
                $text = get_field('news_text');
                $image = wp_get_attachment_image_src(get_field('news_image'), 'news-image');
                ?>

                <div class="container">
                  <div class="row">
                    <div class="col-12 col-md-6">
                      <div class="news-text">
                        <?= $text; ?>
                      </div>
                    </div>
                     <div class="col-md-6">              
                    <div class="news-image">
                        <img src="<?= $image[0]; ?>" class="rounded-circle float-right img-fluid">
                    </div>
                  </div>
                </div>
            <?php
            endforeach;
        else:
            ?>
            <div class="col-xl-12 text-center">
                <h5>Er zijn nog geen nieuwsitems geplaatst.</h5>
            </div>
        <?php
        endif;
        ?>

    </div>

  </div>

</section>