<?php get_template_part('parts/breadcrumbs'); ?>

<section class="news">
  <div class="container">

    <div class="row">
      <div class="col-12 text-center">
        <h2 class="section-header-archive-news">Nieuws</h2>
        
      </div>
    </div>
    <div class="button-group filters-button-group">
          <a href="#" class="button is-checked" data-filter="*">alle</a>
          <a href="#" class="button" data-filter=".zorg">zorg</a>
          <a href="#" class="button" data-filter=".werk">werk</a>
      </div>
    
    <div class="row team-slider">

        <?php
        $args = array (
            'posts_per_page' => - 1,
            'post_status' => 'publish',
            'post_type'   => 'news'
        );
        $get_posts = get_posts( $args );

        if ( $get_posts ):
            foreach ( $get_posts as $post ): ?>

                <?php
                $title = get_field('news_title');
                $date = get_field('news_date');
                $text = get_field('news_text');
                $image = wp_get_attachment_image_src(get_field('news_image'), 'news-image');
                ?>

                <div class="col-5 offset-md-1 grid-item">
                    <div class="news-image">
                        <h3 class="news-title"><?= $title; ?></h3>
                        <span class="news-date"><?= $date; ?></span>
                        <img src="<?= $image[0]; ?>" class="rounded-circle float-right img-fluid">
                    </div>
                    <div class="news-text">
                        <?= $text; ?>
                        <a class="cta-btn cta-orange-news" href="<?php the_permalink(); ?>">Lees meer</a>
                    </div>
                </div>
            <?php
            endforeach;
        else:
            ?>
            <div class="col-xl-12 text-center">
                <h5>Er zijn nog geen nieuwsitems geplaatst.</h5>
            </div>
        <?php
        endif;
        ?>

    </div>

  </div>

</section>