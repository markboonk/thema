<?php while (have_posts()): the_post();

  $header_image = wp_get_attachment_image_src(get_field('home_header_background_image'), 'full');

  if (!$header_image) {
    $header_image = get_stylesheet_directory_uri() . '/dist/images/section-hero-bg.png';
  } else {
    $header_image = $header_image[0];
  }

  ?>

  <section class="hero" style="background-image: url('<?php echo $header_image; ?>');">
    <div class="container">
      <div class="row">
        <div class="order-md-2 col-12 col-md-6">
          <div class="title">
            <span class="first"><?php the_field('home_header_subtitle_1'); ?></span><br>
            <span class="second"><?php the_field('home_header_subtitle_2'); ?></span><br>
            <span class="third"><?php the_field('home_header_subtitle_3'); ?></span><br>
          </div>
          <h1><?php the_field('home_header_title'); ?></h1>
        </div>
        <div class="order-md-1 col-6 col-md-3">
          <a href="<?php the_field('home_header_button_left_url'); ?>"
             class="cta-btn cta-purple"><?php the_field('home_header_button_left_text'); ?></a>
        </div>
        <div class="order-md-3 col-6 col-md-3 text-right">
          <a href="<?php the_field('home_header_button_right_url'); ?>"
             class="cta-btn cta-orange"><?php the_field('home_header_button_right_text'); ?></a>
        </div>
      </div>
    </div>
  </section>
  <section class="about-us">
    <div class="container">
      <div class="row">
        <div class="order-md-2 col-12 col-md-5 text-center">
          <img src="<?php the_field('intro_image'); ?>" class="rounded-circle img-fluid-350">
        </div>
        <div class="order-md-1 col-12 col-md-6">
          <h2 class="section-header"><?php the_field('home_intro_title'); ?></h2>
          <p><?php the_field('home_intro_content'); ?></p>
          <a href="<?php the_field('home_intro_button_url'); ?>"
             class="cta-btn cta-purple"><?php the_field('home_intro_button_text'); ?></a>
        </div>
        <div class="order-md-3 col-md-1">
          <div class="about-bridge">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/section-about-bridge.svg">
          </div>
        </div>
      </div>
    </div>
  </section>


  <?php if (have_rows('home_stories_repeater')) : //Check for content ?>

    <section class="stories">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="section-header"><?php the_field('home_stories_title'); ?></h2>
          </div>
        </div>

        <div class="story-slider">
          <?php while (have_rows('home_stories_repeater')) : the_row(); // Loop through stories
            $image = wp_get_attachment_image_src(get_sub_field('home_stories_photo'), 'full');               // Photo
            $quote = get_sub_field('home_stories_quote');               // Blockquotes
            $text = get_sub_field('home_stories_text');                 // Content
            $name = get_sub_field('home_stories_name');                 // Name
            ?>

            <div class="slide">
              <div class="row">

                <div class="col-12 col-md-4 offset-md-1 stories-image-container">
                  <?php if ($image) : ?>
                    <img src="<?php echo $image[0] ?>" class="rounded-circle stories-image">
                  <?php endif; ?>
                </div>
                <div class="col-12 col-md-6">
                  <?php if ($quote) : ?>
                    <blockquote>
                      <?php echo $quote; ?>
                    </blockquote>
                  <?php endif;
                  if (get_sub_field('home_stories_text')) :
                    echo '<p>' . get_sub_field('home_stories_text') . '</p>';
                  endif;
                  if ($name) : ?>
                    <span class="stories-name">
                     <?php echo $name; ?>
                    </span>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
    </section>
  <?php endif; ?>

  <?php global $post;

  $args = array(
      'posts_per_page' => 2,
      'orderby' => 'date',
      'order' => 'DESC',
      'post_type' => 'news',
      'post_status' => 'publish'
  );
  $news_posts = get_posts($args);

  ?>

  <section class="news">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center">
          <h2 class="section-header"><?php the_field('news_header_home'); ?></h2>
          <span class="news-subtitle">
                    <?php the_field('news_header_subtitle_home'); ?>
                    </span>
        </div>
      </div>
      <div class="row justify-content-center">
        <?php
        foreach ($news_posts as $post) : setup_postdata($post);
          $title = get_field('news_title');
          $date = get_field('news_date');
          $text = get_field('news_short_description');
          $image = wp_get_attachment_image_src(get_field('news_image'), 'news-image');
          ?>
          <div class="col-12 col-md-5 offset-md-1">
            <div class="news-image">
              <h3 class="news-title"><?= $title; ?></h3>
              <span class="news-date"><?= $date; ?></span>
              <img src="<?= $image[0]; ?>" class="rounded-circle float-right img-fluid"/>
            </div>
            <div class="news-text">
              <p>
                <?= $text; ?>
              </p>
              <a class="cta-btn cta-orange"
                 href="<?php the_permalink(); ?>"><?php the_field('button_text_news_item'); ?></a>

            </div>

          </div>
        <?php endforeach;
        wp_reset_postdata(); ?>
      </div>

      <div class="row">
        <div class="col-12 text-center">
          <p><?php the_field('news_footer_home'); ?></p>
          <a href="<?php the_field('news_button_home_url'); ?>"
             class="cta-btn"><?php the_field('news_button_home_text'); ?></a>
        </div>
  </section>
  <?php get_template_part('parts/usp'); ?>
<?php endwhile; ?>