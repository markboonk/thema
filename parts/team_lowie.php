<section class="team">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12 text-center">
                <h2 class="section-header">Team</h2>
            </div>
        </div>
        <div class="team-slider">
            <div class="slide">
                <div class="row">
                    <?php
                    $i = 0;
                    $class = "";
                    foreach($team_posts as $post): $i++;
                    if($i == 1){
                        $class = "all";
                    }
                    if($i == 2){
                        $class = "care";
                    }
                    if($i == 2){
                        $class = "work";
                    }
                    ?>
                        <div class="col-12 col-md-4 team-member all">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/usp.png">
                            <span class="team-name-1">Nick Janssen</span>
                            <p class="team-quote-1">Gewoon een stukkie tekst over deze persoon. Een paar regels is voldoende. Super tof.</p>
                        </div>
                    <?php
                    if($i == 3){
                        $i = 0;
                    }
                    endforeach; ?>
                    <!-- <div class="col-md-1 text-left"><button type="button" onclick="jQuery('.team-slider').slick('slickPrev');"  class="slick-btn slick-btn-prev"><i class="fas fa-chevron-left"></i></button></div> -->

                    <div class="col-12 col-md-4 team-member care">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/usp.png">
                        <span class="team-name-2">Nick Janssen</span>
                        <p class="team-quote-2">Gewoon een stukkie tekst over deze persoon. Een paar regels is voldoende. Super tof.</p>
                    </div>
                    <div class="col-12 col-md-4 team-member work">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/usp.png">
                        <span class="team-name-3">Nick Janssen</span>
                        <p class="team-quote-3">Gewoon een stukkie tekst over deze persoon. Een paar regels is voldoende. Super tof.</p>
                    </div>
                    <!-- <div class="col-md-1 text-right"><button type="button" onclick="jQuery('.team-slider').slick('slickPrev');" class="slick-btn slick-btn-next"><i class="fas fa-chevron-right"></i></button></div> -->
                </div>
            </div>
</section>