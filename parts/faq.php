<?php if ($faq = get_field('faq')): ?>
  <section class="faq">
    <div class="faq-content">
      <div class="container">
        <div class="row">
         <div class= "col-12 text-center">
            <h3 class="faq-header"><?php the_field('faq_header'); ?></h3>
            </div>
            <div class="col-md-8 offset-md-2">
            <?php foreach ($faq as $idx => $item): ?>
              <div class="faq-item">
                <a data-id="<?php echo $idx; ?>" href="#"><?php echo $item['faq_titel']; ?></a>
                <div data-answer-id="<?php echo $idx; ?>" class="faq-answer">
                  <p><?php echo $item['faq_text']; ?></p> 
                </div> 
              </div> 
            <?php endforeach; ?> 
          </div>
        </div>
      </div>  
    </div>  
  </section>  
<?php endif; ?>