<footer class="site-footer">
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div id="footer-address" class="col-md-3 col-12">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/footer-logo.png">
          <address>
           <?php the_field('street_name', 'options'); ?>
            <br> <?php the_field('city_zipcode','options'); ?>
            <br>
            <a href="tel: 0532084088"><?php the_field('phone_number','options'); ?></a>
            <br>
            <a href="mailto:info@visie-groep.nl"><?php the_field('email_adres','options'); ?></a>
          </address>
          <a href="<?php the_field('footer_facebook_url','options'); ?>"><i class="social fab fa-facebook-f"></i></a>
          <a href="<?php the_field('footer_linkedin_url','options'); ?>"><i class="social fab fa-linkedin-in"></i></a>
         </div>
        <div id="footer-menu" class="col-md-3 col-6">
          <span class="title"><?php the_field('footer_menu_header','options'); ?></span>
          <nav class="">
            <?php
            if (has_nav_menu('footer_navigation')):
              wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav']);
            endif;
            ?>
          </nav>
        </div>
        <div id="footer-news" class="col-md-3 col-6">
          <span class="title"><?php the_field('news_header_footer','options'); ?></span>
          <nav class="nav-news">
            <ul>
              <?php
              $args = array(
                  'posts_per_page' => 3,
                  'post_status' => 'publish',
                  'post_type' => 'news'
              );
              $recent_posts = wp_get_recent_posts($args);
              foreach ($recent_posts as $recent) { ?>
                <li>
                  <a href="<?php the_permalink($recent["ID"]); ?>">
                    <?php echo $recent["post_title"]; ?><br>
                    <span class="news-date"><?php echo date('d \- m \- Y', strtotime($recent["post_date"])); ?></span>
                  </a>
                </li>
              <?php }
              wp_reset_query();
              ?>
            </ul>
          </nav>
        </div>
        <div id="footer-newsletter" class="col-md-3 col-12">
          <span class="title"><?php the_field('newsletter_header','options'); ?></span>
          <p class="newsletter"><?php the_field('newsletter_intro','options'); ?></p>
          <form>
            <input type="text" placeholder="e-mailadres">
            <button type="submit"><i class="fas fa-paper-plane"></i></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="container footer-bottom">
    <div class="row">
      <div class="col-12 col-md-10 order-md-2">
        <div class="bottom-navigation">
          <?php
          if (has_nav_menu('footer_bottom_navigation')):
            wp_nav_menu(['theme_location' => 'footer_bottom_navigation', 'menu_class' => 'nav']);
          endif;
          ?>
        </div>
      </div>
      <div class="copyright col-12 col-md-2 order-md-1"><?php the_field('copyright', 'options'); ?> &copy;
        <?php echo date('Y'); ?>
      </div>
    </div>
  </div>
