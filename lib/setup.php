<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup()
{
  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  //Yoast Support
  add_theme_support('yoast-seo-breadcrumbs');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage'),
    'footer_navigation' => __('Footer Navigation', 'sage'),
    'footer_bottom_navigation' => __('Footer Bottom Navigation', 'sage')
  ]);

    add_image_size( 'news-image', 286, 286, true ); // 220 pixels wide by 180 pixels tall, hard crop mode


  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');
    add_image_size("news-image", 286, 286);

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));

  // Custom logo
  $logo_args = array(
      'height' => 60,
      'width' => 190,
    'flex-height' => false,
      'flex-width' => false,
      'header-text' => array('site-title', 'site-description'),
  );
    add_theme_support('custom-logo', $logo_args);

  // Add theme support for Custom Header
    $header_args = array(
        'width' => 1920,
        'height' => 600,
        'flex-width' => false,
        'flex-height' => false,
        'uploads' => true,
        'random-default' => false,
        'header-text' => false,
    );
    add_theme_support('custom-header', $header_args);
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');



/**
 * Register sidebars
 */
function widgets_init()
{
  register_sidebar([
      'name' => __('Primary', 'sage'),
      'id' => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
      'after_widget' => '</section>',
      'before_title' => '<h3>',
      'after_title' => '</h3>'
  ]);

  register_sidebar([
      'name' => __('Footer', 'sage'),
      'id' => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
      'after_widget' => '</section>',
      'before_title' => '<h3>',
      'after_title' => '</h3>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');



/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar()
{
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-custom.php'),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets()
{
  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

# Attach Custom Post Types and Custom Taxonomies
add_action('init', __NAMESPACE__ . '\\attach_post_types', 0);
function attach_post_types()
{
    # Attach Custom Post Types
    include_once(THEME_DIR . 'options\post-types.php');

    # Attach Custom Taxonomies
    include_once(THEME_DIR . 'options\taxonomies.php');
}

/**
 * Add options page
 */
if (function_exists('acf_add_options_page')) {
  acf_add_options_page(array(
      'page_title' => 'Website instellingen',
      'menu_title' => 'Website instellingen',
      'menu_slug' => 'theme-general-settings',
      'capability' => 'edit_posts',
      'redirect' => false
  ));

  acf_add_options_page(array(
    'page_title' => 'Footer',
    'menu_title' => 'Footer',
    'menu_slug' => 'theme-general-settings',
    'capability' => 'edit_posts',
    'redirect' => false

  ));  
}
