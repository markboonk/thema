<section class="team">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <h2 class="section-header">
          Team
        </h2>
      </div>
    </div>

      <div class="button-group filters-button-group">
          <a href="#" class="button is-checked" data-filter="*">alle</a>
          <a href="#" class="button" data-filter=".zorg">zorg</a>
          <a href="#" class="button" data-filter=".werk">werk</a>
      </div>

    <div class="row team-slider">
      <?php
      global $post;
      $args = array(
          'posts_per_page' => -1,
          'post_type' => 'team',
          'post_status' => 'publish'
      );
      $team_posts = get_posts($args);
      if ($team_posts) :
        foreach ($team_posts as $post) : setup_postdata($post);
          $name = get_field('team_name');
          $section = get_field('team_section');
          $description = get_field('team_description');
          $image = wp_get_attachment_image_src(get_field('team_image'), array(286, 286));
          ?>

          <div class="col-12 col-md-4 grid-item <?php foreach ($section as $single_section): print $single_section . ' '; endforeach; ?>">
            <div class="team-member <?php foreach($section as $single_section): print $single_section . ' '; endforeach; ?>">
              <div class="rounded-circle">
              <img src="<?= $image[0]; ?>" class="rounded-circle img-fluid">
              </div>
              <span class="name"><?= $name ?></span>
              <p class="description"><?= $description ?></p>
              <div class="team-socials">
                <a href="<?php the_field('linkedin_url_team_member'); ?>"><i class="fab fa-linkedin-in"></i></a>
                <a href="mailto:<?php the_field('email_url_team_member'); ?>"><i class="far fa-envelope"></i></a>
                <a href="<?php the_field('twitter_url_team_member'); ?>"><i class="fab fa-twitter"></i></a>
              </div>
            </div>
          </div>

        <?php endforeach;
      endif;
      ?>
    </div>
  </div>
</section>
