<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.css' rel='stylesheet'/>
<div class="container">
  <?php the_content(); ?>
</div>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
<?php get_template_part('parts/breadcrumbs'); ?>

<!-- TODO Email link stylen -->
<!-- TODO Google maps met API key, maps is rechts pagina breed! -->


<section class="contact">
  <div id="map">
  </div>
  <!--        <iframe-->
  <!--            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2443.7267767204744!2d6.8709966160812534!3d52.23018157976053!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b813edd0fe4521%3A0x3ac2fb183fec1421!2sHengelosestraat+298%2C+7521+AM+Enschede!5e0!3m2!1snl!2snl!4v1542639007785"-->
  <!--            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen>-->
  <!--        </iframe>-->
  <div class="container">
    <div class="row">

      <div class="col-12 col-md-4 order-md-1" style="background-color:;">
        <h2 class="section-header adres"><?php the_field('adres_header'); ?></h2>
        <address>
          <?php the_field('street_name', 'options'); ?>
          <br> <?php the_field('city_zipcode','options'); ?>
          <br>
          <a href="tel: 0532084088"><?php the_field('phone_number','options'); ?></a>
          <br>
          <a href="mailto:info@visie-groep.nl"><?php the_field('email_adres','options'); ?></a>
        </address>
        <h2 class="section-header form"><?php the_field('form_header'); ?></h2>
        <?php echo do_shortcode('[contact-form-7 id="242" title="Contact form 1"]'); ?>
      </div>
      <script>
          mapboxgl.accessToken = 'pk.eyJ1IjoidmlzaWVncm9lcCIsImEiOiJjanB3bG8yb3kxMGNrNDlzMm9pZHFuaXd4In0.XyrS5jUGfrYcLwPRVOClvA';
          var map = new mapboxgl.Map({
              container: 'map', // container id
              style: 'mapbox://styles/visiegroep/cjpwloszt048t2sqss5q7803o', // stylesheet location
              center: [6.872280, 52.230600], // starting position [lng, lat]
              zoom: 16 // starting zoom
          });
      </script>


</section>

