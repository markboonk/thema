<?php ?>
<div class="container">
  <?php the_content(); ?>
</div>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

<?php get_template_part('parts/breadcrumbs'); ?>

<section class="services">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-5 order-md-2 text-center" style="background-color: ;">
        <img src="<?php the_field('image_area_1'); ?>" class="rounded-circle img-fluid-350">
      </div>
      <div class="col-12 col-md-6" style="background-color:;">
        <h2 class="zorg"><?php the_field('title_area_1_zorg'); ?></h2>
          <p><?php the_field('text_area_1'); ?></p>
          <a href="<?php the_field('services_button_url_1'); ?>"
             class="cta-btn"><?php the_field('services_button_text_1'); ?></a>
      </div>
      <div class="col-12 col-md-1 order-md-1" style="background-color:;">
      </div>
      <div class="col-12 col-md-5 colom_4 order-md-3 text-center" style="background-color: ;">
        <img src="<?php the_field('image_area_2'); ?>" class="rounded-circle img-fluid-350">
      </div>
      <div class="col-12 col-md-7 colom_5 order-md-4" style="background-color:;">
        <h2 class="zorg"><?php the_field('title_area_2_zorg'); ?></h2>
          <p class="text-rechts"><?php the_field('text_area_2'); ?></p>
          <a href="<?php the_field('services_button_url_2'); ?>"
             class="cta-btn"><?php the_field('services_button_text_2'); ?></a>
      </div>
    </div>
  </div>
</section>

<?php get_template_part('parts/quotes'); ?>

<?php get_template_part('parts/flow'); ?>

<?php get_template_part('parts/faq'); ?>

<?php get_template_part('parts/usp'); ?>





